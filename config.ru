require 'sinatra'
require './homepage'

require "sinatra/cache_assets"
use Sinatra::CacheAssets, :max_age => 3600

# Rack config
use Rack::CommonLogger

map "/" do
  run Homepage
end
