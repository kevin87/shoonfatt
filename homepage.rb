require 'sinatra'

class Homepage < Sinatra::Base
  set :public, File.dirname(__FILE__) + '/static'

  get "/" do
    headers['Cache-Control'] = "public, max-age=3600"

    file = "index.html"
    if File.exists?("static/" + file)
      return File.open("static/" + file)
    else
     return "error"
    end
  end
end