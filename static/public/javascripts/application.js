// Plugin for the rotating popup boxes
$.fn.cycle = function(timeout){
    var $all_elem = $(this)

    show_cycle_elem = function(index){
        if(index == $all_elem.length) index = 0; //you can make it start-over, if you want
        $all_elem.fadeOut(800).eq(index).fadeIn(800)
        setTimeout(function(){show_cycle_elem(++index)}, timeout);
    }
    show_cycle_elem(0);
}

$(document).ready(function() {
  
  $("div.group_products a").lightBox();
  
  $("a.category").click(function() {
    $("div.category_wrapper").fadeOut(800);
    $("div.group_products").fadeIn(800);
    $("div.group_products a").lightBox();
  });
  
  $("div.previous_product").hover(
    function() {
      $(this).animate({
          left: '-60px'
      }, 200)
    },
    function() {
      $(this).animate({
          left: '-25px'
      }, 200)
    }
  );
  
  $("div.next_product").hover(
    function() {
      $(this).animate({
          right: '-62px'
      }, 200)
    },
    function() {
      $(this).animate({
          right: '-25px'
      }, 200)
    }
  );
  
  $("div.banner img").cycle(5000);
  
  // ScrollTo codes start

  var $scrollerWindow = $("body");
  var $speed = 1500;
  
  function gotoHome() {
    $scrollerWindow.stop().scrollTo( $('section.home'), $speed, {axis:'y',offset:{left: 0, top:0 }} );
  };
  
  function gotoAbout() {
    $scrollerWindow.stop().scrollTo( $('section.about'), $speed, {axis:'y',offset:{left: 0, top:0 }} );
  };
  
  function gotoProducts() {
    $scrollerWindow.stop().scrollTo( $('section.products'), $speed, {axis:'y',offset:{left: 0, top:0 }} );
  };
  
  function gotoCareers() {
    $scrollerWindow.stop().scrollTo( $('section.career'), $speed, {axis:'y',offset:{left: 0, top:0 }} );
  };
  
  function gotoContact() {
    $scrollerWindow.stop().scrollTo( $('section.contact'), $speed, {axis:'y',offset:{left: 0, top:0 }} );
  };
  
  $('a.home, a#logo_link').live("click", gotoHome);
  $('a.about').live("click", gotoAbout);
  $('a.products').live("click", gotoProducts);
  $('a.careers').live("click", gotoCareers);
  $('a.contact').live("click", gotoContact);
  
  // ScrollTo codes end

  $('.scroll-pane').jScrollPane();

  // Cache the Window object
  	$window = $(window);

  	// Cache the Y offset and the speed of each sprite
  	$('[data-type]').each(function() {	
  		$(this).data('offsetY', parseInt($(this).attr('data-offsetY')));
  		$(this).data('Xposition', $(this).attr('data-Xposition'));
  		$(this).data('speed', $(this).attr('data-speed'));
  	});

  	// For each element that has a data-type attribute
  	$('section[data-type="background"]').each(function(){


  		// Store some variables based on where we are
  		var $self = $(this),
  			offsetCoords = $self.offset(),
  			topOffset = offsetCoords.top,
  			$link = $self.attr('rel');

  		// When the window is scrolled...
  	  $(window).scroll(function() {

  			// If this section is in view
  			if ( ($window.scrollTop() + $window.height()) > (topOffset) &&
  				 ( (topOffset + $self.height()) > $window.scrollTop() ) ) {

  				// Scroll the background at var speed
  				// the yPos is a negative value because we're scrolling it UP!								
  				var yPos = -($window.scrollTop() / $self.data('speed')); 

  				// If this element has a Y offset then add it on
  				if ($self.data('offsetY')) {
  					yPos += $self.data('offsetY');
  				}

  				// Put together our final background position
  				var coords = '0% '+ yPos + 'px';

  				// Move the background
  				$self.css({ backgroundPosition: coords });

  				// Check for other sprites in this section	
  				$('[data-type="sprite"]', $self).each(function() {

  					// Cache the sprite
  					var $sprite = $(this);

  					// Use the same calculation to work out how far to scroll the sprite
  					var yPos = -($window.scrollTop() / $sprite.data('speed'));					
  					var coords = $sprite.data('Xposition') + ' ' + (yPos + $sprite.data('offsetY')) + 'px';

  					$sprite.css({ backgroundPosition: coords });													

  				}); // sprites

  				$("nav ul li a").removeClass("current");
  				$($link).addClass("current");

  					

  			}; // in view

  		}); // window scroll

  	});	// each data-type
});